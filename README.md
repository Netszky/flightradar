# A TESTER

A Tester sur : **_iOS pour de meilleurs performances_**.

# FlightRadar

Projet de Groupe M1 Dev Web -> React Native

- Leo Peyre
- Julien Chigot
- Aaron Saksik

## Issues

### Android

- Notifications
- Splash Screen Design
- Bottom Tab Navigation Bug ?

## A propos du projet

Création d'une appli de tracking du traffic aérien avec React-Native / Redux.
Inspiré de : https://www.flightradar24.com/

## Fonctionnalités

- Multilangue
- Notifications (Notifee)
- Redux (CLEAN)
- Splash Screen iOS / Android
- Google Map / Apple
  - Marker
  - Marker On Press
  - Refresh On region change
  - Choix de limite
  - Aéroports
- Modal
  - Detail Avions
- Thème en fonction des préférences du tel
  - Il faut désactiver le mode debug pour qu'il puisse get le mode du tel, fermer l'app et la réouvrir

## Getting started

### Android

- git clone https://gitlab.com/Netszky/flightradar.git
- npm install

### iOS

- git clone https://gitlab.com/Netszky/flightradar.git
- npm install
- pod install

## A Faire

- Check Connexion Internet
- Retour sur Erreur
- Animations ?
