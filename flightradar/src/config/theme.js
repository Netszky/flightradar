const lightTheme = {
    background: '#ffffff',
    font: 'black',
    logoBgd: '#656565',
    navigationBgd: '#f4f4f4',
    navigationFontFocus: '#2d8aa7',
    navigationFont: 'black'
}

const darkTheme = {
    background: '#787878',
    font: '#f4f4f4',
    logoBgd: '#656565',
    navigationBgd: '#424242',
    navigationFontFocus: '#2d8aa7',
    navigationFont: 'white'

}

export { lightTheme, darkTheme }
