import notifee, { AuthorizationStatus } from '@notifee/react-native'

const handleNotificationPermissions = async () => {
    const settings = await notifee.requestPermission()
    if (settings.authorizationStatus === AuthorizationStatus.DENIED) {
        console.log('User denied permissions request')
        showMessage({
            message: i18next.t('errors.noNotifications'),
            description: i18next.t('errors.noNotificationsDescription'),
            type: 'danger',
            icon: 'auto',
            duration: 11000,
            onPress: () => Linking.openSettings()
        })
    } else if (
        settings.authorizationStatus === AuthorizationStatus.AUTHORIZED
    ) {
        console.log('User granted permissions request')
    } else if (
        settings.authorizationStatus === AuthorizationStatus.PROVISIONAL
    ) {
        console.log('User provisionally granted permissions request')
    }
    return (
        <></>
    )
}

export default handleNotificationPermissions