import axios from 'axios';


export const GET_PLANE_REQUEST = 'GET_PLANE_REQUEST';
export const GET_PLANE_SUCCESS = 'GET_PLANE_SUCCESS';
export const GET_PLANE_FAIL = 'GET_PLANE_FAIL';

export const GET_REGION_PLANE_REQUEST = 'GET_REGION_PLANE_REQUEST';
export const GET_REGION_PLANE_SUCCESS = 'GET_REGION_PLANE_SUCCESS';
export const GET_REGION_PLANE_FAIL = 'GET_REGION_PLANE_FAIL';


export const getRegion = (region, distance, limit) => async dispatch => {
  dispatch({ type: GET_REGION_PLANE_REQUEST });
  await axios
    .get('https://aviation-edge.com/v2/public/flights?key=6a90c7-b60e71', {
      params: {
        lat: region.latitude,
        lng: region.longitude,
        distance: distance,
        limit: limit,
      },
    })
    .then(data => {
      dispatch({ type: GET_REGION_PLANE_SUCCESS, payload: data.data });
    })
    .catch(err => dispatch({ type: GET_REGION_PLANE_FAIL, payload: err }));


};

export const getPlanes = (limit) => (dispatch) => {
  dispatch({ type: GET_PLANE_REQUEST });
  axios
    .get('https://aviation-edge.com/v2/public/flights?key=6a90c7-b60e71', {
      params: {
        limit: limit,
      },
    })
    .then(data => {
      dispatch({ type: GET_PLANE_SUCCESS, payload: data.data ? data.data : [] });
    })
    .catch(err => { dispatch({ type: GET_PLANE_FAIL, payload: err }) });

};
