import axios from "axios";

export const GET_AIRLINE_REQUEST = 'GET_ARLINE_REQUEST';
export const GET_AIRLINE_SUCCESS = 'GET_AIRLINE_SUCCESS';
export const GET_AIRLINE_FAIL = 'GET_AIRLINE_FAIL';

export const getAirLine = (code) => async (dispatch) => {
    dispatch({
        type: GET_AIRLINE_REQUEST
    })
    await axios
        .get(
            'https://aviation-edge.com/v2/public/airlineDatabase?key=6a90c7-b60e71',
            {
                params: {
                    codeIataAirline: code,
                },
            },
        )
        .then(data => {
            dispatch({ type: GET_AIRLINE_SUCCESS, payload: data.data[0] });
        })
        .catch(err => dispatch({ type: GET_AIRLINE_FAIL, payload: err }));

}

