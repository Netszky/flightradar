import axios from 'axios';

export const GET_ARRIVAL_AIRPORT_REQUEST = 'GET_ARRIVAL_AIRPORT_REQUEST';
export const GET_ARRIVAL_AIRPORT_SUCCESS = 'GET_ARRIVAL_AIRPORT_SUCCESS';
export const GET_ARRIVAL_AIRPORT_FAIL = 'GET_ARRIVAL_AIRPORT_FAIL';

export const getArrivalAirport = (code) => async (dispatch) => {
  dispatch({ type: GET_ARRIVAL_AIRPORT_REQUEST })
  try {
    await axios
      .get(
        'https://aviation-edge.com/v2/public/airportDatabase?key=6a90c7-b60e71',
        {
          params: {
            codeIataAirport: code,
          },
        },
      )
      .then(data => {
        dispatch({ type: GET_ARRIVAL_AIRPORT_SUCCESS, payload: data.data[0] });
      })
      .catch(err => console.log(err));
  } catch (error) {
    dispatch({ type: GET_ARRIVAL_AIRPORT_FAIL, payload: error })
  }
};
