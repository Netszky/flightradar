import React, { useEffect, useState, useRef } from 'react';
import MapView, { Marker } from 'react-native-maps';
import styled from 'styled-components';
import FlightModal from '../../components/flightmodal';
import Svg, { Path } from 'react-native-svg';
import { PermissionsAndroid } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { getPlanes, getRegion } from '../../actions/plane';
import { getAirLine } from '../../actions/modal';
import { getDepartureAirport } from '../../actions/departureAirport';
import { getArrivalAirport } from '../../actions/arrivalAirport';
import LimitButton from '../../components/limitbutton';

const HomeScreen = () => {
  const dispatch = useDispatch();
  const [selected, setSelected] = useState();
  const [limit, setLimit] = useState(10);
  const [button, setButton] = useState(1);
  async function requestGeolocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'FlightRadar Geolocation Permission',
          message:
            'FlightRadar need your permission',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the geolocation');
      } else {
        console.log('Geolocation permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }
  const airline = useSelector(state => state.modal);
  const departureAP = useSelector(state => state.departureAirport);
  const arrivalAP = useSelector(state => state.arrivalAirport);
  const plane = useSelector(state => state.plane);
  const [isModalVisible, setModalVisible] = useState(false);

  // const { loading: loadingAirline, data: dataAirline } = airline;
  const { loading, data, error } = plane;
  const { loading: loadingAP, airportAP: dataAP, error: errorAP } = arrivalAP;
  const { loading: loadingDA, departureAP: dataDA, error: errorDA } = departureAP;
  const fetchLimit = (limit) => {
    dispatch(getPlanes(limit));
    setLimit(limit)
  }
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  useEffect(() => {

    requestGeolocationPermission();
    dispatch(getPlanes(limit))
  }, [dispatch]);

  return (
    <>
      <MapView
        style={{ width: '100%', height: '100%' }}
        rotateEnabled={false}
        mapType="standard"
        maxZoomLevel={5}
        loadingEnabled={true}
        showsUserLocation={true}
        onRegionChangeComplete={region => {

          dispatch(getRegion(region, 3000, limit))

        }
        }
      >
        {data &&
          data.map(plane => {
            return (
              <>
                <Marker key={plane.flight.number}
                  onPress={() => {
                    dispatch(getArrivalAirport(plane.arrival.iataCode)),
                      dispatch(getDepartureAirport(plane.departure.iataCode)),
                      setSelected(plane)
                    toggleModal(),
                      dispatch(getAirLine(plane.airline.iataCode))
                  }}
                  coordinate={{
                    latitude: plane.geography.latitude,
                    longitude: plane.geography.longitude,
                    longitudeDelta: 1,
                    latitudeDelta: 1
                  }}
                  title={plane.aircraft.iataCode}>
                  <Plane d={plane.geography.direction - 45}>
                    <Svg viewBox="0 0 24 24">
                      <Path
                        fill="rgb(205,0,0)"
                        d="M20.56 3.91C21.15 4.5 21.15 5.45 20.56 6.03L16.67 9.92L18.79 19.11L17.38 20.53L13.5 13.1L9.6 17L9.96 19.47L8.89 20.53L7.13 17.35L3.94 15.58L5 14.5L7.5 14.87L11.37 11L3.94 7.09L5.36 5.68L14.55 7.8L18.44 3.91C19 3.33 20 3.33 20.56 3.91Z"
                      />
                    </Svg>
                  </Plane>
                </Marker>
                {dataAP &&
                  <Marker
                    coordinate={{
                      latitude: dataAP.latitudeAirport,
                      longitude: dataAP.longitudeAirport,
                    }}>
                    <CustomMarker>
                      <Svg viewBox="0 0 24 24">
                        <Path
                          fill="#fff"
                          d="M14.4,6H20V16H13L12.6,14H7V21H5V4H14L14.4,6M14,14H16V12H18V10H16V8H14V10L13,8V6H11V8H9V6H7V8H9V10H7V12H9V10H11V12H13V10L14,12V14M11,10V8H13V10H11M14,10H16V12H14V10Z"
                        />
                      </Svg>
                    </CustomMarker>
                  </Marker>
                }
                {dataDA &&
                  <Marker
                    coordinate={{
                      latitude: dataDA.latitudeAirport,
                      longitude: dataDA.longitudeAirport,
                    }}></Marker>
                }
              </>
            );
          })}
      </MapView >
      {selected &&
        <FlightModal plane={selected} isVisible={isModalVisible} onPress={toggleModal} />
      }
      <SliderContainer>
        <Container>

          <LimitButton test={fetchLimit} limit={"10"} id={1}></LimitButton>
          <LimitButton test={fetchLimit} limit={"30"} id={2}></LimitButton>
          <LimitButton test={fetchLimit} limit={"50"} id={3}></LimitButton>
          <LimitButton test={fetchLimit} limit={"100"} id={4}></LimitButton>
        </Container>
        <LimitText>Avions : {limit}</LimitText>
      </SliderContainer>
    </>
  );
};
const Plane = styled.View`
  width: 60px;
  height: 30px;
  justify-content: center;
  align-items: center;
  transform: rotate(${props => props.d}deg);
`;
const CustomMarker = styled.View`
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
`;
const SliderContainer = styled.View`
  width: 100%;
  height: 30px;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 10%;
  position: absolute;
`
const Container = styled.View`
  display: flex;
  flex-direction: row;
`
const LimitText = styled.Text`
  color: ${props => props.theme.font};
  font-weight: bold;
  font-size: 20px;
  padding: 0px 30px;
  background-color: ${props => props.theme.background};
`

export default HomeScreen;
