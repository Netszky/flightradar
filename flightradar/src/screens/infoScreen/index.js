import React, { useEffect, useState } from 'react';
import SplashScreen from 'react-native-splash-screen'
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import Logo from '../../components/logo/index';
import SwitchLanguage from '../../components/switch/language/index'

const InfoScreen = () => {
  const { t } = useTranslation();
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <>
      <Container>
        <Logo />
        <SwitchLanguage />
        <Content>
          <Title>{t('info.title1')}</Title>
          <Text>{t('info.text1')}</Text>
          <Text>{t('info.text2')}</Text>
          <Title>{t('info.title2')}</Title>
          <Text>{t('info.text3')}</Text>
        </Content>
      </Container>
    </>
  );
};
const Container = styled.View`
  height: 100%;
  background-color: ${props => props.theme.background};
`;

const Content = styled.View`
  justify-content: center;
  align-items: flex-start;
`;

const Title = styled.Text`
  margin: 40px 10px 10px 10px;
  justify-content: flex-start;
  font-size: 20px;
  font-weight: bold;
  color: ${props => props.theme.font};
`;
const Text = styled.Text`
  margin: 10px;
  padding: 0px 30px;
  justify-content: flex-start;
  font-size: 13px;
  font-weight: 400;
  color: ${props => props.theme.font};
`;

export default InfoScreen;
