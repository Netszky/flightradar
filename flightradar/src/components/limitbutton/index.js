import React, { useState } from 'react';
import styled from 'styled-components';

const LimitButton = (props) => {
    return (
        <Button onPress={() => props.test(props.limit)}>
            <Text >{props.limit}</Text>
        </Button>
    );
};

const Button = styled.TouchableOpacity`
    width: 80px;
    height: 60px;
    border-radius: 9px;
    border: 2px solid #000;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 10px;
    background-color: ${props => props.theme.background};
`
const Text = styled.Text`
    color: ${props => props.theme.font};
    font-weight: bold;
    font-size: 20px;
`
export default LimitButton;