import React from 'react';
import styled from 'styled-components';
import {Dimensions} from 'react-native';

const Logo = () => {
  const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

  return (
    <LogoView width={SCREEN_WIDTH}>
      <LogoImg source={require('../../assets/flight-radar.png')} />
    </LogoView>
  );
};

const LogoView = styled.View`
  width: ${props => props.width}px;
  height: 120px;
  align-items: center;
  justify-content: center;
  border-bottom-color: grey;
  border-bottom-width: 1px;
  margin-bottom: 30px;
  background-color: ${props => props.theme.navigationBgd};
`;

const LogoImg = styled.Image`
  margin-top: 40px;
  width: 200px;
  height: 110px;
`;
export default Logo;
