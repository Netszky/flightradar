import React, { useEffect, useState, useRef } from 'react';
import styled from 'styled-components';
import Modal from "react-native-modal";
import { useSelector } from 'react-redux';

const FlightModal = (props) => {
    const airline = useSelector(state => state.modal);
    const departure = useSelector(state => state.departureAirport);
    const { loading: loadingDA, departureAP, error: errorDA } = departure;
    const arrivalAP = useSelector(state => state.arrivalAirport);
    const { loading: loadingAP, airportAP, error: errorAP } = arrivalAP;
    const { loading, data, error } = airline;

    return (
        <ViewModal>
            <Modal
                style={{ margin: 20 }}
                animationInTiming={300}
                animationIn="slideInUp"
                animationOut="slideOutDown"
                isVisible={props.isVisible}
                onBackdropPress={props.onPress}
            >
                <ViewInModal>
                    {
                        !loading &&
                        <ViewTitle>
                            <NameFlight>{props.plane.aircraft.regNumber}</NameFlight>
                            <CompagnyFlight>{data.nameAirline}</CompagnyFlight>
                        </ViewTitle>
                    }
                    <Image
                        source={{
                            uri: 'https://cdn-lejdd.lanmedia.fr/var/europe1/storage/images/lejdd/politique/avion-comment-le-discours-de-macron-et-du-gouvernement-a-radicalement-change-sur-les-vols-interieurs-3981363/55780839-3-fre-FR/Avion-comment-le-discours-de-Macron-et-du-gouvernement-a-radicalement-change-sur-les-vols-interieurs.jpg',
                        }} />
                    <ViewInfo>
                        <InfoAirport>
                            {departureAP &&
                                <>
                                    <RefAirport>{departureAP.codeIataAirport}</RefAirport>
                                    <NameAirport>{departureAP.nameCountry}</NameAirport>
                                </>
                            }
                            <InfoHour>
                                <TitleHour>Programmé:</TitleHour>
                                <Hour>17:15</Hour>
                            </InfoHour>
                            <InfoHour>
                                <TitleHour>Actuel:</TitleHour>
                                <Hour>17:15</Hour>
                            </InfoHour>
                        </InfoAirport>
                        <InfoAirport>
                            {airportAP &&
                                <>
                                    <RefAirport>{airportAP.codeIataAirport}</RefAirport>
                                    <NameAirport>{airportAP.nameCountry}</NameAirport>
                                </>
                            }
                            <InfoHour>
                                <TitleHour>Programmé:</TitleHour>
                                <Hour>22:30</Hour>
                            </InfoHour>
                            <InfoHour>
                                <TitleHour>Estimé:</TitleHour>
                                <Hour>00:01</Hour>
                            </InfoHour>
                        </InfoAirport>
                    </ViewInfo>
                </ViewInModal>
            </Modal>
        </ViewModal >
    );
};

const ButtonNotif = styled.TouchableOpacity`
background-color: blue;
`
const TextNotif = styled.Text`
`
const ViewModal = styled.View`
background-color: black;
`
const ViewInModal = styled.ScrollView`
    width: 100%;
    height: 60%;
    background-color: #333333;
    position: absolute;
    bottom: 0px;
    border-radius: 20px;
`
const ViewTitle = styled.View`
    background-color: #333333;
    width: 100%;
    border-top-right-radius: 20px;
    border-top-left-radius: 20px;
`
const NameFlight = styled.Text`
    font-size: 35px;
    padding: 10px 0px 5px 10px;
    color: rgb(205,0,0);
`
const CompagnyFlight = styled.Text`
    font-size: 20px;
    color: white;
    padding: 0px 0px 5px 10px;
`
const ViewInfo = styled.View`
    width: 100%;
    flex-direction: row;
    background-color: #e5e5e5;
`
const InfoAirport = styled.View`
    width: 50%;
    align-items: center;
    border: 2px solid white;
`
const RefAirport = styled.Text`
    font-size: 50px;
    font-weight: 600;
`
const NameAirport = styled.Text`
    font-size: 20px;
`
const InfoHour = styled.View`
    font-size: 20px;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    border: 1px solid white;
    background-color: #b3b3b3;
    padding: 6px;
`
const TitleHour = styled.Text`
    font-size: 15px;
    margin: auto;
`
const Hour = styled.Text`
    font-size: 20px;
`
const Image = styled.Image`
    width: 100%;
    height: 220px;

`
const TextButton = styled.Text`
    font-size: 20px;
`
const Button = styled.TouchableOpacity`
    align-items: flex-end;
    padding: 10px;
`
export default FlightModal;