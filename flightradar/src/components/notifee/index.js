import React, { useEffect, useRef } from 'react';
import { AppState } from "react-native";
import notifee, { TriggerType } from '@notifee/react-native';
import handleNotificationPermissions from '../../utils/handleNotifPersmissions';
import { useTranslation } from 'react-i18next';

const Notifee = () => {
    const { t, i18n } = useTranslation();
    const appState = useRef(AppState.currentState);
    useEffect(() => {
        handleNotificationPermissions()
        AppState.addEventListener("change", nextAppState => {
            const date = new Date(Date.now());
            const trigger = {
                type: TriggerType.TIMESTAMP,
                timestamp: date.setSeconds(date.getSeconds() + 2)
            }

            if (appState.current === "inactive") {
                notifee.createTriggerNotification(
                    {
                        title: t('notifee.title'),
                        body: t('notifee.text'),
                        android: {
                            channelId: 'your-channel-id',
                        },
                    },
                    trigger,
                );
            }
            appState.current = nextAppState;
        });
    }, []);
}
export default Notifee;