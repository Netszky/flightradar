import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';

const SwitchLanguage = (props) => {
    const { t, i18n } = useTranslation();
    const [isEnabled, setIsEnabled] = useState(false);

    useEffect(() => {
        i18n.changeLanguage('en');
    }, []);

    const translate = language => {
        if (language == 'fr') {
            i18n.changeLanguage('en');
            setIsEnabled(previousState => !previousState);
        } else {
            i18n.changeLanguage('fr');
            setIsEnabled(previousState => !previousState);
        }
    };

    return (
        <ContentSwitch>
            <TextLanguage>🏴󠁧󠁢󠁥󠁮󠁧󠁿</TextLanguage>
            <Switch
                trackColor={{ false: '#767577', true: '#2d8aa7' }}
                thumbColor={'#f4f3f4'}
                ios_backgroundColor="#3e3e3e"
                onValueChange={() => translate(i18n.language)}
                value={isEnabled}
            />
            <TextLanguage>🇫🇷</TextLanguage>
        </ContentSwitch>
    );
};

const Switch = styled.Switch`
`

const ContentSwitch = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin: 10px;
`;

const TextLanguage = styled.Text`
  padding: 0px 10px;
  font-size: 25px;
`;
export default SwitchLanguage;