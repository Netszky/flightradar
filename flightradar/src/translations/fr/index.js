export default {
  translation: {
    info: {
      title1: 'À quoi servons-nous ?',
      text1:
        'Ici vous aurez la possibilité de voir en temps réel, les divers avions au dessus de votre tête.',
      text2:
        "De suivre leurs trajets, de voir leurs aéroports de depart ainsi que d'arrivés",
      title2: 'Instructions :',
      text3:
        'Pour observer un avion, il faut simplement le selectionner lire les information affichés',
    },
    notifee: {
      title: 'Sur la tête de ma race reviens',
      text: "T'a trop pris la confiance"
    }
  },
};
