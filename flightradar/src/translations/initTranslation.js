import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

import en from './en/index';
import fr from './fr/index';

const resources = {
  en,
  fr,
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'en',
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
