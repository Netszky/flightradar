export default {
  translation: {
    info: {
      title1: 'What are we for ?',
      text1:
        'Here you will be able to see in real time, the various planes above your head.',
      text2:
        'Follow their routes, see their airports of departure and arrival.',
      title2: 'Instructions :',
      text3:
        'To observe an aircraft, simply select it and read the information displayed.',
    },
    notifee: {
      title: 'On the head of my race return',
      text: 'You took too much confidence'
    }
  },
};
