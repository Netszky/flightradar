import React from 'react';
import HomeScreen from '../screens/homeScreen';
import InfoScreen from '../screens/infoScreen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { store } from './store';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconMCI from 'react-native-vector-icons/MaterialCommunityIcons';

const Routes = (props) => {
  const Tab = createBottomTabNavigator();
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={{
            tabBarOptions: {
              showLabel: false,
            },
            tabBarStyle: { backgroundColor: props.theme.navigationBgd},
          }}
        >
          <Tab.Screen
            name="Radar"
            component={HomeScreen}
            options={{
              headerShown: false,
              tabBarIcon: ({ focused }) => (
                <IconMCI
                  name="radar"
                  size={30}
                  color={focused ? props.theme.navigationFontFocus : props.theme.navigationFont}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Information"
            component={InfoScreen}
            options={{
              headerShown: false,
              tabBarIcon: ({ focused }) => (
                <IconAntDesign
                  name="info"
                  size={30}
                  color={focused ? props.theme.navigationFontFocus : props.theme.navigationFont}
                />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default Routes;
