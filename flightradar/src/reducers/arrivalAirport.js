import { GET_ARRIVAL_AIRPORT_FAIL, GET_ARRIVAL_AIRPORT_REQUEST, GET_ARRIVAL_AIRPORT_SUCCESS } from "../actions/arrivalAirport";

const initialState = {
  airportAP: {},
};

export default (state = { initialState, loading: true }, action) => {
  switch (action.type) {
    case GET_ARRIVAL_AIRPORT_REQUEST:
      return { loading: true };
    case GET_ARRIVAL_AIRPORT_SUCCESS:
      return { ...state, airportAP: action.payload, loading: false };
    case GET_ARRIVAL_AIRPORT_FAIL:
      return { loading: false, error: action.payload }
    default:
      return state;
  }
};
