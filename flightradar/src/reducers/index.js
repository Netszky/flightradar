import { combineReducers } from 'redux';

import arrivalAirport from './arrivalAirport';
import departureAirport from './departureAirport';
import plane from './plane';
import modal from './modal';

export default combineReducers({
  arrivalAirport,
  departureAirport,
  plane,
  modal

});
