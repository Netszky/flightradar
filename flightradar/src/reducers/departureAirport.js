import { GET_DEPARTURE_AIRPORT_REQUEST, GET_DEPARTURE_AIRPORT_SUCCESS, GET__DEPARTURE_AIRPORT_FAIL } from "../actions/departureAirport";

const initialState = {
    departureAP: {},
};

export default (state = { initialState, loading: true }, action) => {
    switch (action.type) {
        case GET_DEPARTURE_AIRPORT_REQUEST:
            return { loading: true };
        case GET_DEPARTURE_AIRPORT_SUCCESS:
            return { ...state, departureAP: action.payload, loading: false };
        case GET__DEPARTURE_AIRPORT_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
};
