import { GET_PLANE_REQUEST, GET_PLANE_SUCCESS, GET_PLANE_FAIL, GET_REGION_PLANE_FAIL, GET_REGION_PLANE_REQUEST, GET_REGION_PLANE_SUCCESS, } from '../actions/plane';

const initialState = {
  data: [],
};

export default (state = { initialState, loading: true }, action) => {
  switch (action.type) {
    case GET_REGION_PLANE_REQUEST:
      return { ...state, loading: true };
    case GET_REGION_PLANE_SUCCESS:
      return { ...state, data: action.payload, loading: false };
    case GET_REGION_PLANE_FAIL:
      return { ...state, loading: false, error: action.payload };

    case GET_PLANE_REQUEST:
      return { ...state, loading: true };
    case GET_PLANE_SUCCESS:
      return { ...state, data: action.payload, loading: false };
    case GET_PLANE_FAIL:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
