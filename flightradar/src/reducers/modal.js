import { GET_AIRLINE, GET_AIRLINE_FAIL, GET_AIRLINE_REQUEST, GET_AIRLINE_SUCCESS } from "../actions/modal";

const initialState = {
    data: [],
};

export default (state = { initialState, loading: true }, action) => {
    switch (action.type) {
        case GET_AIRLINE_REQUEST:
            return { loading: true };
        case GET_AIRLINE_SUCCESS:
            return { ...state, data: action.payload, loading: false };
        case GET_AIRLINE_FAIL:
            return { loading: false, error: action.payload }
        default:
            return state;
    }
};
