import React, { useEffect } from 'react';
import Routes from './src/routes/index';
import './src/translations/initTranslation';
import Notifee from './src/components/notifee/index';
import SplashScreen from 'react-native-splash-screen';
import { ThemeProvider } from 'styled-components'
import { lightTheme, darkTheme } from './src/config/theme'
import { Appearance } from 'react-native';


const App = () => {
  const isDarkMode = Appearance.getColorScheme() === 'dark';
  Notifee(); 

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <ThemeProvider theme={isDarkMode ? darkTheme : lightTheme} >
      <Routes theme={isDarkMode ? darkTheme : lightTheme}/>
    </ThemeProvider>
  );
};
export default App;
